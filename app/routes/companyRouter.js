const express = require("express");

const router = express.Router();

const {
    getAllCompanyMiddleware,
    createCompanyMiddleware,
    getCompanyByIDMiddleware,
    updateCompanyMiddleware,
    deleteCompanyMiddleware
} = require("../middlewares/companyMiddleware");

class Company {
    _id;
    _company;
    _contact;
    _country;

    constructor(paramId, paramCompany, paramContact, paramCountry) {
        this._id = paramId;
        this._company = paramCompany;
        this._contact = paramContact;
        this._country = paramCountry;
    }
}

var vCompanies = [
    new Company(1, "Alfreds Futterkiste", "Maria Anders", "Germany"),
    new Company(2, "Centro comercial Moctezuma", "Francisco Chang", "Mexico"),
    new Company(3, "Ernst Handel", "Roland Mendel", "Austria"),
    new Company(4, "Island Trading", "Helen Bennett", "UK"),
    new Company(5, "Laughing Bacchus Winecellars", "Yoshi Tannamuri", "Canada"),
    new Company(6, "Magazzini Alimentari Riuniti", "Giovanni Rovelli", "Italy"),
];

router.get("/", getAllCompanyMiddleware, (req, res) => {
    res.json({
        companies: vCompanies
    })
});

router.post("/", createCompanyMiddleware, (req, res) => {
    res.json({
        message: "POST company"
    })
})

router.get("/:companyId", getCompanyByIDMiddleware, (req, res) => {
    const companyId = req.params.companyId;

    res.json({
        message: "GET company id = " + companyId
    })
})

router.put("/:companyId", updateCompanyMiddleware, (req, res) => {
    const companyId = req.params.companyId;

    res.json({
        message: "PUT company id = " + companyId
    })
})

router.delete("/:companyId", deleteCompanyMiddleware, (req, res) => {
    const companyId = req.params.companyId;

    res.json({
        message: "DELETE company id = " + companyId
    })
})

module.exports = router;

