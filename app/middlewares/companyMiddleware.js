const getAllCompanyMiddleware = (req, res, next) => {
    console.log("GET all companies middleware");

    next();
}

const createCompanyMiddleware = (req, res, next) => {
    console.log("POST company middleware");

    next();
}

const getCompanyByIDMiddleware = (req, res, next) => {
    console.log("GET company by id middleware");

    next();
}

const updateCompanyMiddleware = (req, res, next) => {
    console.log("PUT company middleware");

    next();
}

const deleteCompanyMiddleware = (req, res, next) => {
    console.log("DELETE company middleware");

    next();
}

module.exports = {
    getAllCompanyMiddleware,
    createCompanyMiddleware,
    getCompanyByIDMiddleware,
    updateCompanyMiddleware,
    deleteCompanyMiddleware
}
