//B1: Import thư viện express
//import express from express
const express = require('express');

//B2: Khởi tạo app express
const app = new express();

//B3: Khai báo cổng để chạy api
const port = 8000;

const companyRouter = require("./app/routes/companyRouter");

// Sử dụng router 
app.use("/api/v1/companies", companyRouter);

//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
